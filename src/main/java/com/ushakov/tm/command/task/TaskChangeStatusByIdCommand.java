package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Change task status by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Task task = serviceLocator.getTaskService().changeStatusById(userId, taskId, status);
    }

    @Override
    @NotNull
    public String name() {
        return "change-task-status-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
