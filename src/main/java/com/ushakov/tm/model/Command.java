package com.ushakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Command {

    @Nullable
    private String arg = "";

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    public Command(@NotNull String name, @Nullable String arg, @Nullable String description) {
        this.arg = arg;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        String result = "";
        result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}