package com.ushakov.tm.exception.entity;

import com.ushakov.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task was not found!");
    }

}
