package com.ushakov.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}
