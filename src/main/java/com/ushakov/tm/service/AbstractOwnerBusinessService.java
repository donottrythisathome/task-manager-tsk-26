package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IOwnerBusinessRepository;
import com.ushakov.tm.api.service.IOwnerBusinessService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.empty.EmptyUserIdException;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractOwnerBusinessService<E extends AbstractOwnerBusinessEntity>
        extends AbstractOwnerService<E>
        implements IOwnerBusinessService<E> {

    @NotNull
    private final IOwnerBusinessRepository<E> repository;

    public AbstractOwnerBusinessService(@NotNull final IOwnerBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @NotNull
    public E findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final E entity = repository.findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(ObjectNotFoundException::new);
        return entity;
    }

    @Override
    @Nullable
    public E removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeOneByName(userId, name);
    }

    @Override
    @NotNull
    public E changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findOneById(userId, id);
        entity.setStatus(status);
        return entity;
    }

    @Override
    @NotNull
    public E changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final E entity = findOneByIndex(userId, index);
        entity.setStatus(status);
        return entity;
    }

    @Override
    @NotNull
    public E changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @NotNull final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findOneByName(userId, name);
        entity.setStatus(status);
        return entity;
    }

    @Override
    @NotNull
    public E completeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    @NotNull
    public E completeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final E entity = findOneByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    @NotNull
    public E completeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findOneByName(userId, name);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    @NotNull
    public E startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findOneById(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    @NotNull
    public E startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final E entity = findOneByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    @NotNull
    public E startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findOneByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    @NotNull
    public E updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findOneById(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    @NotNull
    public E updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findOneByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}