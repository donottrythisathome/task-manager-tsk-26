package com.ushakov.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class Project extends AbstractOwnerBusinessEntity {

    public Project(@NotNull String name) {
        this.name = name;
    }

    @Override
    @NotNull
    public String toString() {
        return "ID: " + id + "\nNAME: " + name + "\nDESCRIPTION: " + description
                + "\nSTATUS: " + status.getDisplayName() + "\nSTART DATE: " + dateStart
                + "\nCREATED: " + created;
    }

}